

var Monedas_1=["Moneda destino", "Dólar Estadounidense ","Dólar Canadiense","Euro"];
var Monedas_2=["Moneda destino", "Pesos Mexicanos","Dólar Canadiense","Euro"];
var Monedas_3=["Moneda destino", "Pesos Mexicanos","Dólar Estadounidense ","Euro"];
var Monedas_4=["Moneda destino", "Pesos Mexicanos","Dólar Estadounidense ","Dólar Canadiense"];
var totgene=0;
function cambia() {
    var Origen= document.op.monedaOrg[document.op.monedaOrg.selectedIndex].value;
    
    if (Origen != 0) {
        Optenido = eval("Monedas_" + Origen);
        num = Optenido.length;
        document.op.monedaDes.length = num;
        for (i = 0; i < num; i++) {
            document.op.monedaDes.options[i].value = Optenido[i];
            document.op.monedaDes.options[i].text = Optenido[i];
        }
    } else {
        document.op.monedaDes.length = 1;
        document.op.monedaDes.options[0].value = "-";
        document.op.monedaDes.options[0].text = "-";
    }
    document.op.monedaDes.options[0].selected = true;
    


}
var monedaOrg = "";
var monedaDes= "";
var cantidad = "";
var subtotal = "";
var totComision = "";
var totPagar;

function Calcular() {
    cantidad=document.getElementById("cantidad").value;
    monedaOrg=Number(document.op.monedaOrg[document.op.monedaOrg.selectedIndex].value);
    monedaDes=String(document.op.monedaDes[document.op.monedaDes.selectedIndex].value) ;
    
    if (monedaDes=="Moneda destino" || monedaOrg==0 || cantidad<1) {
        alert("Selecciona una moneda o cantidad mayor que 0");
    }
    else{
        subtotal=Number((Subtotal(cantidad,monedaOrg,monedaDes)).toFixed(2)) ;
        totComision=Number(Comision(subtotal).toFixed(2)) ;
        totPagar=Number(totP(subtotal,totComision)) ;
        document.getElementById("subtotal").value=subtotal;
        document.getElementById("TotComi").value=totComision;
        document.getElementById("TotPag").value=totPagar.toFixed(2); 
    }
    
}
function Subtotal(cant,org,des) {
    if(org==2 && des=="Pesos Mexicanos"){
      return cant*19.85;
    }
    if(org==2 && des=="Dólar Canadiense"){
      return cant*1.35;
    }
    if(org==2 && des=="Euro"){
      return cant*0.99;
    }
    if(org==1 && des=="Dólar Estadounidense "){
      return cant/19.85;
    }
    if(org==1 && des=="Dólar Canadiense"){
      return (cant/19.85)*1.35;
    }
    if(org==1 && des=="Euro"){
      return (cant/19.85)*0.99;
    }
    if(org==3 && des=="Dólar Estadounidense "){
      return cant/1.35;
    }
    if(org==3 && des=="Pesos Mexicanos"){
      return (cant*19.85)/1.35;
    }
    if(org==3 && des=="Euro"){
      return (cant*0.99)/1.35;
    }
    if(org==4 && des=="Dólar Estadounidense "){
      return (cant/0.99);
    }
    if(org==4 && des=="Pesos Mexicanos"){
      return (cant*19.85)/0.99;
    }
    if(org==4 && des=="Dólar Canadiense"){
      return (cant*1.35)/0.99;
    }

    
}
function Comision(sub) {
  return sub*0.03;
}
function totP(sub,com) {
  return sub+com;
}
function Registrar() {
    let acu=document.getElementById('TotPag').value;
    cantidad=document.getElementById("cantidad").value;
    monedaOrg=String(document.op.monedaOrg[document.op.monedaOrg.selectedIndex].text);
    monedaDes=String(document.op.monedaDes[document.op.monedaDes.selectedIndex].value) ;
    if (monedaDes=="Moneda destino" || monedaOrg==0 || cantidad<1) {
        alert("Selecciona una moneda o cantidad mayor que 0");
    }
    else{
        var sub=document.getElementById("subtotal").value;
        var com=document.getElementById("TotComi").value;
        var tot=document.getElementById("TotPag").value;
        
        totgene=parseFloat(acu)+totgene;
        
        const lista=document.querySelector("#ul");
        const li=document.createElement("li");
        li.textContent="Cantidad:     "+cantidad+"   Moneda origen:    "+monedaOrg+"   Moneda destino:    "+monedaDes+"   Subtotal:    "+sub+"   Comision:    "+com+"   Total a pagar:    "+tot;
        lista.appendChild(li);
        document.getElementById("totalgen").innerText=totgene.toFixed(0)+"    $";
    }
    
    
}
function borrar() {
    const lista=document.querySelector("#ul");
    lista.remove();
    alert("Registros eliminados correctamente");
    location.reload();
    
    
}
function Numeros(string){//Solo numeros
    var out = '';
    var filtro = '1234567890';//Caracteres validos
	
    //Recorrer el texto y verificar si el caracter se encuentra en la lista de validos 
    for (var i=0; i<string.length; i++)
       if (filtro.indexOf(string.charAt(i)) != -1) 
             //Se añaden a la salida los caracteres validos
	     out += string.charAt(i);
	
    //Retornar valor filtrado
    return out;
} 